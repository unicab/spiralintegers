//
//  ViewController.swift
//  SpiralIntegers
//
//  Created by Tam Nguyen on 1/10/18.
//  Copyright © 2018 Tam Nguyen. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        guard let spiral = SpriralUnit(n: 4) else { return }
        spiral.generateSpiral()
        spiral.rowUnits.forEach { print($0.values) }
    }
}

// [1  2  3  4  5]
// [0  0  0  0  0]
// [0  0  0  0  0]
// [0  0  0  0  0]
// [0  0  0  0  0]

class SpriralUnit {
    let n: Int
    private(set) var rowUnits = [RowUnit]()
    
    init?(n: Int) { guard n>0 else { return nil }; self.n = n }
    
    func generateSpiral() {
        (1...n).forEach { _ in rowUnits.append(RowUnit(length: n)) }
        let _ = self.rowUnits.first?.fill()
        
        for _ in 1...2*n-1 {
            self.rowUnits = self.rotate()
            for row in rowUnits { if row.fill() { break } }
        }
    }
    
    private func rotate() -> [RowUnit] {
        var newRowUnit = [RowUnit]()
        
        for i in 1...rowUnits.count {
            let newRow = rowUnits.flatMap { $0.values[rowUnits.count-i] }
            newRowUnit.append(RowUnit(ints: newRow))
        }
        
        return newRowUnit
    }
}

class RowUnit {
    private(set) var values: [Int]
    
    init(length: Int) { values = [Int](repeatElement(0, count: length)) }
    init(ints: [Int]) { values = ints }
    
    func fill() -> Bool {
        guard let targetIndex = values.index(of: 0) else { return false }
        
        for i in targetIndex...values.count-1 {
            guard values[i] == 0 else { break }
            values[i] = (i==0) ? 1 : values[i-1] + 1
        }
        
        return true
    }
}
